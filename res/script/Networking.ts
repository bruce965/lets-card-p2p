/// <reference path="defs/jquery/jquery.d.ts" />
/// <reference path="defs/peerjs/peerjs.d.ts" />
/// <reference path="util/Dom.ts" />

module letscard {

    export class Networking {

        private peer: PeerJs.Peer;
        private inProgressConnections: string[] = [];
        private connections: PeerJs.DataConnection[] = [];

        private id: JQuery;
        private peers: JQuery;
        private connectBtn: JQuery;

        public onMessage: (sender: PeerJs.DataConnection, data: any) => void;

        constructor() {
            var peer = this.peer = new Peer({ key: '6xm78vdhxtievcxr' });

            peer.on('open', id => {
                this.onIdChanged(id);
            });

            peer.on('connection', connection => {
                this.onConnection(connection);
            });

            peer.on('close', () => {
                this.onIdChanged(null);
                peer.destroy();
            });

            peer.on('disconnect', () => {
                this.onIdChanged(null);
                peer.reconnect();
            });

            peer.on('error', error => {
                if (error.type == 'peer-unavailable') {
                    this.onConnectionFailed(error.message);
                    return;
                }

                if (error.type == 'webrtc')
                    return;

                this.fatalError(error.message);
            });

            this.buildInterface();
        }

        private addPeer(id: string) {
            for (var i = 0; i < this.inProgressConnections.length; i++)
                if (this.inProgressConnections[i] == id)
                    return;

            var connection = this.peer.connect(id, {
                reliable: true
            });
            this.onConnection(connection);
        }

        private serviceMessage(action: string, data: Object) {
            data['__svc'] = '__letscard';
            data['action'] = action;
            this.send(data);
        }

        private buildInterface(): void {
            dom.make('div', {
                class: ['networking'],
                child: [
                    this.id = dom.make('div', {
                        class: ['networking-id']
                    }),
                    this.peers = dom.make('div', {
                        class: ['networking-peers']
                    })
                ]
            }).appendTo(document.body);

            this.onIdChanged(null);
            this.onNetworkUpdated();
        }

        private onIdChanged(id: string): void {
            if (id)
                this.id.text("Your ID: " + id);
            else
                this.id.text("Your ID: -offline-");
        }

        private onNetworkUpdated(): void {
            var ids: string[] = ["you"];

            for (var i = 0; i < this.connections.length; i++)
                ids.push(this.connections[i].peer);

            this.peers.text("Your network: " + ids.join(', ') + '.');
        }

        private fatalError(error: string): void {
            this.onIdChanged(null);
            alert("Fatal Error: " + error);
        }

        private onConnectionFailed(message: string): void {
            alert(message);
        }

        private onConnection(connection: PeerJs.DataConnection): void {
            this.inProgressConnections.push(connection.peer);

            connection.on('open', () => {
                //console.log("open");

                this.serviceMessage('addPeer', { id: connection.peer });

                connection.on('data', data => {
                    if (data['__svc'] == '__letscard') {
                        switch (data.action) {
                            case 'addPeer':
                                this.addPeer(data.id);
                                break;
                        }
                        return;
                    }

                    if (this.onMessage)
                        this.onMessage(connection, data);
                });

                this.connections.push(connection);
                this.onNetworkUpdated();
            });

            connection.on('close', () => {
                //console.log("close");

                for (var i = 0; i < this.inProgressConnections.length; i++) {
                    if (this.inProgressConnections[i] == connection.peer) {
                        this.inProgressConnections.splice(i, 1);
                        break;
                    }
                }

                for (var i = 0; i < this.connections.length; i++) {
                    if (this.connections[i] == connection) {
                        this.connections.splice(i, 1);
                        break;
                    }
                }

                this.onNetworkUpdated();
            });

            connection.on('error', error => {
                console.log("Error from connection with peer", connection.peer, error);

                for (var i = 0; i < this.inProgressConnections.length; i++) {
                    if (this.inProgressConnections[i] == connection.peer) {
                        this.inProgressConnections.splice(i, 1);
                        break;
                    }
                }
            });
        }

        public addFriend(): void {
            var id = prompt("Insert the ID of the peer you want to connect to:");

            if (!id)
                return;

            this.addPeer(id);
        }

        public send(data: any, alsoSelf: boolean = false): void {
            //console.debug("Broadcasting message", data);

            for (var i = 0; i < this.connections.length; i++)
                this.connections[i].send(data);

            if (alsoSelf)
                this.onMessage(null, data);
        }
    }
}
