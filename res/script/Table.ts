/// <reference path="defs/jquery/jquery.d.ts" />
/// <reference path="util/ContextMenu.ts" />
/// <reference path="util/Delay.ts" />
/// <reference path="util/Dom.ts" />
/// <reference path="util/Noise.ts" />
/// <reference path="util/Resizable.ts" />

module letscard {

    export class Table {

        public space: JQuery;
        private table: JQuery;

        private lastWidth: number;
        private lastHeight: number;
        private contextMenu: ContextMenu;
        private spawnCards: ContextMenu;
        private cardCategories: ContextMenu;
        private cardCategoriesRemove: ContextMenu;
        private cardsRemove: ContextMenu;
        private tables: ContextMenu;
        private tablesDelete: ContextMenu;

        public onResize: (width: number, height: number) => void;
        public onAddFriend: () => void;
        public onNewGame: () => void;
        public onSpawnCard: (x: number, y: number, key: string) => void;
        public onAddCardCategory: (name: string, category: CardCategory) => void;
        public onRemoveCardCategory: (key: string) => void;
        public onAddCard: (name: string, card: CardData) => void;
        public onRemoveCard: (key: string) => void;
        public onSaveTable: (key: string) => void;
        public onLoadTable: (key: string) => void;
        public onDeleteTable: (key: string) => void;
        public onResetTable: () => void;

        private onResizeInstance = () => this.onResizeLowLevel(false);

        constructor() {
            this.buildInterface();
        }

        private buildInterface(): void {
            var resizeDelay = new Delay(200);
            var setDelay = () => resizeDelay.set();

            this.table = dom.make('div', {
                class: ['table'],
                style: {
                    backgroundImage: 'url(' + generateNoise(256, 256, 0.05) + ')'
                },
                on: {
                    resize: setDelay
                }
            }).appendTo(document.body);

            this.space = dom.make('div', {
                class: ['table-space'],
                style: {
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0
                }
            }).appendTo(this.table);

            new Resizable(this.table);

            this.contextMenu = new ContextMenu(this.table, {
                "Game": {
                    "Add friend to table": () => this.onAddFriend(),
                    "Start new game": () => this.onNewGame(),
                    "Spawn card": this.spawnCards = new ContextMenu(null, {}),
                },
                '-0-': null,
                "Editing": {
                    "Card categories": {
                        "Add": () => this.onAddCardCategoryLowLevel(),
                        "Remove": this.cardCategoriesRemove = new ContextMenu(null, {})
                    },
                    "Cards": {
                        "Add": this.cardCategories = new ContextMenu(null, {}),
                        "Remove": this.cardsRemove = new ContextMenu(null, {})
                    },
                    "Table": {
                        "Save": () => { var name = prompt("Save table with name:"); if (name) this.onSaveTable(name); },
                        "Load": this.tables = new ContextMenu(null, {}),
                        "Delete": this.tablesDelete = new ContextMenu(null, {}),
                        "Reset": () => this.onResetTable()
                    }
                },
                '-1-': null,
                "Let's Card, p2p edition": null,
                "by Fabio Iotti": () => window.open('http://fabiogiopla.altervista.org/', '_blank')
            });

            resizeDelay.onDelay = () => this.onResizeLowLevel(true);
            this.onResizeLowLevel(true);
        }

        private onResizeLowLevel(direct: boolean): void {
            var newWidth = this.table.width();
            var newHeight = this.table.height();

            if (this.lastWidth == newWidth && this.lastHeight == newHeight)
                return;

            this.lastWidth = newWidth;
            this.lastHeight = newHeight;

            if (direct && this.onResize)
                this.onResize(this.lastWidth, this.lastHeight);
        }

        private onAddCardCategoryLowLevel(): void {
            var result = prompt("Card category?\n" +
                `{
    width: number;
    height: number;

    roundness?: number;
    thickness?: number;
    color?: string;
    background?: string;

    miniature?: boolean;
}`, `{
    "width": 100,
    "height": 150,

    "roundness": 5,
    "thickness": 0,
    "color": "gray",
    "background": "http://cdn.example.com/yourBackImage.png",

    "miniature": false
}`);
            if (!result)
                return;

            var category: CardCategory;
            try {
                category = JSON.parse(result);
            } catch (e) {
                alert(e);
                return;
            }

            var name = prompt("Card category name?", "myCardCategory");
            if (!name)
                return;

            this.onAddCardCategory(name, category);
        }

        private onAddCardLowLevel(category: string): void {
            var result = prompt("Card data?\n" +
                `{
    category: string; (` + category + `)

    image?: string;
}`, `{
    "image": "http://cdn.example.com/yourFrontImage.png"
}`);
            if (!result)
                return;

            var card: CardData;
            try {
                card = JSON.parse(result);
            } catch (e) {
                alert(e);
                return;
            }

            var name = prompt("Card name?", "myCard");
            if (!name)
                return;

            card.category = category;
            this.onAddCard(name, card);
        }

        public clear(): void {
            this.space.empty();
        }

        public setSize(width: number, height: number): void {
            this.table.stop().animate({
                width: width,
                height: height
            }).promise().done(this.onResizeInstance);
        }

        public setCardCategories(categories: { [key: string]: CardCategory }): void {
            var entries: ContextMenuEntries = {};
            var removeEntries: ContextMenuEntries = {};

            var added = false;
            for (var k in categories) {
                ((category: CardCategory, k: string) => {
                    entries[k] = () => this.onAddCardLowLevel(k);
                    removeEntries[k] = () => this.onRemoveCardCategory(k);
                })(categories[k], k);

                added = true;
            }

            if (!added) {
                entries["No card category!"] = null;
                removeEntries["No card category!"] = null;
            }

            this.cardCategories.setEntries(entries);
            this.cardCategoriesRemove.setEntries(removeEntries);
        }

        public setCards(cards: { [key: string]: CardData }) {
            var entries: ContextMenuEntries = {};
            var spawnEntries: ContextMenuEntries = {};
            var added = false;
            for (var k in cards) {
                ((card: CardData, k: string) => {
                    entries[k] = () => this.onRemoveCard(k);
                    spawnEntries[k] = () => this.onSpawnCard(
                        this.contextMenu.getOpenLocation().x - this.table.position().left,
                        this.contextMenu.getOpenLocation().y - this.table.position().top,
                        k
                        );
                })(cards[k], k);

                added = true;
            }

            if (!added) {
                entries["No card!"] = null;
                spawnEntries["No card!"] = null;
            }

            this.cardsRemove.setEntries(entries);
            this.spawnCards.setEntries(spawnEntries);
        }

        public setTables(tables: string[]) {
            var entries: ContextMenuEntries = {};
            var entriesDelete: ContextMenuEntries = {};
            var added = false;
            for (var i in tables) {
                ((table: string) => {
                    entries[table] = () => this.onLoadTable(table);
                    entriesDelete[table] = () => this.onDeleteTable(table);
                })(tables[i]);

                added = true;
            }

            if (!added) {
                entries["No saved table!"] = null;
                entriesDelete["No saved table!"] = null;
            }

            this.tables.setEntries(entries);
            this.tablesDelete.setEntries(entriesDelete);
        }
    }
}
