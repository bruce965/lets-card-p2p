/// <reference path="defs/jquery/jquery.d.ts" />
/// <reference path="util/ContextMenu.ts" />
/// <reference path="util/Dom.ts" />
/// <reference path="util/Draggable.ts" />
/// <reference path="util/Noise.ts" />
/// <reference path="Table.ts" />

module letscard {

    export interface CardCategory {
        width: number;
        height: number;

        roundness?: number;
        thickness?: number;
        color?: string;
        background?: string;

        miniature?: boolean;
    }

    export interface CardData {
        category: string;

        image?: string;
    }

    var leftToRightGradient = generateHorizontalGradient(0.05, 0.2);
    var rightToLeftGradient = generateHorizontalGradient(0.2, 0.05);

    export class Card {

        public card: JQuery;

        private lastTop: number;
        private lastLeft: number;

        public onMove: (x: number, y: number) => void;
        public onFlip: (showing: boolean) => void;
        public onRemove: () => void;

        private onMoveInstance = () => this.onMoveLowLevel(false);

        constructor(table: Table, private category: CardCategory, private data: CardData) {
            this.card = dom.make('div', {
                class: ['card'],
                style: {
                    position: 'absolute',
                    width: category.width,
                    height: category.height + (category.thickness || 0),
                    //background: 'rgba(255, 0, 0, 0.5)',    // DEBUG
                    borderRadius: category.roundness || 0
                }
            }).appendTo(table.space);

            if (category.thickness)
                this.card.css({ zIndex: Math.floor(category.thickness * 10), transform: 'translateZ(' + category.thickness + 'px)' });

            var container = dom.placeholder(['card-container']).appendTo(this.card);

            var content = dom.placeholder(['card-content']).appendTo(container);

            var back = dom.make('div', {
                class: ['card-back'],
                style: {
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: category.thickness || 0,
                    right: 0,

                    borderRadius: category.roundness || 0,
                    backgroundColor: category.color || 'gray',
                    backgroundImage: category.background ? 'url(' + category.background + ')' : '',
                    backgroundSize: '100% 100%'
                }
            }).appendTo(content);

            if (!category.miniature) {
                dom.make('div', {
                    class: ['card-front'],
                    style: {
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        bottom: category.thickness || 0,
                        right: 0,

                        borderRadius: category.roundness || 0,
                        backgroundColor: category.color || 'gray',
                        backgroundImage: data.image ? 'url(' + data.image + ')' : '',
                        backgroundSize: '100% 100%'
                    }
                }).appendTo(content);
            }

            if (category.thickness) {
                dom.make('div', {
                    class: ['card-thickness'],
                    style: {
                        position: 'absolute',
                        left: 0,
                        bottom: 0,
                        right: 0,
                        height: (category.thickness || 0) + (category.roundness || 0),

                        borderRadius: '0 0 ' + (category.roundness || 0) + 'px ' + (category.roundness || 0) + 'px',
                        backgroundColor: category.color || 'gray',
                        backgroundImage: 'url(' + leftToRightGradient + '), url(' + rightToLeftGradient + ')',
                        backgroundRepeat: 'no-repeat, no-repeat',
                        backgroundPosition: '0, 100%',
                        backgroundSize: '50% 100%, 50% 100%'
                    }
                }).insertBefore(back);
            }

            var draggable = new Draggable(this.card, container, true);
            draggable.onDrop = () => {
                this.onMove(this.card.position().left, this.card.position().top);
            };

            new ContextMenu(container, {
                "Flip card": category.miniature ? null : () => this.flip(undefined, false, true),
                "Remove": () => {
                    this.remove(true);
                }
            });
        }

        public flip(visible: boolean, noAnimate: boolean, manually: boolean): void {
            if (this.category.miniature)
                return;

            var flipped = visible !== undefined ? visible : !this.card.is('.card-flipped');
            this.card.appendTo(this.card.parent()).addClass(noAnimate ? null : 'animate').toggleClass('card-flipped', flipped).toggleClass('card-unflipped', !flipped);

            //console.debug("flip", visible, flipped);

            if (manually && this.onFlip)
                this.onFlip(!!flipped);
        }

        public remove(manually: boolean): void {
            this.card.remove();

            if(manually && this.onRemove)
                this.onRemove();
        }

        private onMoveLowLevel(direct: boolean): void {
            var newPosition = this.card.position();

            if (this.lastTop == newPosition.top && this.lastLeft == newPosition.left)
                return;

            this.lastTop = newPosition.top;
            this.lastLeft = newPosition.left;

            if (direct && this.onMove)
                this.onMove(this.lastLeft, this.lastTop);
        }

        public setPosition(x: number, y: number) {
            this.card.stop().animate({
                top: y,
                left: x
            }).promise().done(this.onMoveInstance);
        }
    }
}
