

module letscard {

    /** A predictable random number generator. */
    export class Random {

        private w: number;
        private z: number;

        constructor() {
            this.seed(Math.floor(Math.random() * 0x100000000));
        }

        /** Seed this random number generator. */
        public seed(seed: number): void {
            this.w = seed;
            this.z = 987654321;
        }

        /** Return a random number between 0 (inclusive) and 1 (exclusive). */
        public normalized(): number {
            this.z = (36969 * (this.z & 0xffff) + (this.z >> 16)) & 0xffffffff;
            this.w = (18000 * (this.w & 0xffff) + (this.w >> 16)) & 0xffffffff;
            return (((this.z << 16) + this.w) & 0xffffffff) / 0x100000000 + 0.5;
        }

        /** Return a random integer number between min (inclusive) and max (inclusive). */
        public intBetween(min: number, max: number) {
            return Math.floor(min + this.normalized() * (max - min + 1));
        }

        /** Return a random number between min (inclusive) and max (exclusive). */
        public between(min: number, max: number) {
            return min + this.normalized() * (max - min);
        }
    }
}
