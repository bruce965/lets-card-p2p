
module letscard {

    var randomString = () => Math.floor(Math.random() * 0x100000000000).toString(32);

    var randomId = randomString() + randomString();

    export function uid() {
        return new Date().getTime().toString(32) + randomId + randomString();
    }
}
