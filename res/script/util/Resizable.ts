/// <reference path="../defs/jquery/jquery.d.ts" />
/// <reference path="Dom.ts" />

module letscard {

    export class Resizable {

        private elements: JQuery;

        constructor(private el: JQuery) {
            if(el.css('position') == 'static')
                el.css({ position: 'relative' });

            var resizing = (e: JQueryMouseEventObject, horiz: boolean, vert: boolean) => {
                var initialWidth = el.width();
                var initialHeight = el.height();
                var initialX = e.clientX;
                var initialY = e.clientY;

                var onMove = e => {
                    move(initialWidth + (horiz ? (e.clientX - initialX) : 0), initialHeight + (vert ? (e.clientY - initialY) : 0));
                    e.preventDefault();
                };

                var onUp = e => {
                    onMove(e);

                    $(window).off('mousemove', onMove).off('mouseup', onUp);
                    overlay.detach();
                };

                $(window).on('mousemove', onMove).on('mouseup', onUp);
                overlay.css({ cursor: $(e.target).css('cursor') }).appendTo(document.body);

                e.preventDefault();
            };

            var move = (width: number, height: number) => {
                el.css({
                    width: width,
                    height: height
                });
                el.resize();
            };

            var overlay = dom.make('div', {
                style: {
                    position: 'fixed',
                    top: 0,
                    right: 0,
                    bottom: 0,
                    left: 0,
                    //background: 'blue', opacity: 0.5,    // DEBUG
                }
            });

            var right = dom.make('div', {
                style: {
                    position: 'absolute',
                    top: 0,
                    right: -10 - parseFloat(el.css('border-right-width')) || 0,
                    width: 10 + parseFloat(el.css('border-right-width')) || 0,
                    height: '100%',
                    //background: 'red', opacity: 0.5,    // DEBUG
                    cursor: 'e-resize'
                },
                on: {
                    mousedown: e => resizing(e, true, false)
                }
            }).appendTo(el);

            var bottom = dom.make('div', {
                style: {
                    position: 'absolute',
                    bottom: -10 - parseFloat(el.css('border-bottom-width')) || 0,
                    left: 0,
                    width: '100%',
                    height: 10 + parseFloat(el.css('border-bottom-width')) || 0,
                    //background: 'red', opacity: 0.5,    // DEBUG
                    cursor: 's-resize'
                },
                on: {
                    mousedown: e => resizing(e, false, true)
                }
            }).appendTo(el);

            var bottomRight = dom.make('div', {
                style: {
                    position: 'absolute',
                    bottom: -10 - parseFloat(el.css('border-bottom-width')) || 0,
                    right: -10 - parseFloat(el.css('border-right-width')) || 0,
                    width: 10 + parseFloat(el.css('border-right-width')) || 0,
                    height: 10 + parseFloat(el.css('border-bottom-width')) || 0,
                    //background: 'green', opacity: 0.5,    // DEBUG
                    cursor: 'se-resize'
                },
                on: {
                    mousedown: e => resizing(e, true, true)
                }
            }).appendTo(el);

            this.elements = $([overlay, right, bottomRight, bottom]);
        }

        public disable() {
            this.elements.hide();
        }

        public enable() {
            this.elements.show();
        }

        public destroy() {
            this.elements.remove();
            this.elements = null;
        }
    }
}
