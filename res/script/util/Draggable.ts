/// <reference path="../defs/jquery/jquery.d.ts" />
/// <reference path="Dom.ts" />

module letscard {

    export class Draggable {

        private static translateZ = 0;

        private elements: JQuery;

        public onDrop: () => void;

        constructor(private el: JQuery, activeArea: JQuery = el, bringToFrontOnDrag: boolean = false) {
            if (el.css('position') == 'static')
                el.css({ position: 'absolute' });

            var dragging = (e: JQueryMouseEventObject) => {
                if (e.which != 1)
                    return;

                var initial = el.position();
                var initialX = e.clientX;
                var initialY = e.clientY;

                var onMove = e => {
                    move(initial.top + e.clientY - initialY, initial.left + e.clientX - initialX);
                    e.preventDefault();
                };

                var onUp = e => {
                    onMove(e);

                    $(window).off('mousemove', onMove).off('mouseup', onUp);
                    el.removeClass('dragging');
                    overlay.css({ position: 'absolute' });
                    if(this.onDrop)
                        this.onDrop();
                };

                $(window).on('mousemove', onMove).on('mouseup', onUp);
                el.addClass('dragging').removeClass('animate');
                overlay.css({ position: 'fixed', transform: 'translateZ(' + (Draggable.translateZ++ / 1000000) + 'px)' });

                if (bringToFrontOnDrag)
                    el.appendTo(el.parent());

                e.preventDefault();
            };

            var move = (top: number, left: number) => {
                if (top < 0)
                    top = 0;
                else if (top > this.el.parent().height() - el.outerHeight())
                    top = this.el.parent().height() - el.outerHeight();

                if (left < 0)
                    left = 0;
                else if (left > this.el.parent().width() - el.outerWidth())
                    left = this.el.parent().width() - el.outerWidth();

                el.css({
                    top: top,
                    left: left
                });
                el.resize();
            };

            var overlay = dom.make('div', {
                class: ['object-3d'],
                style: {
                    position: 'absolute',
                    top: -parseFloat(el.css('border-top-width')) || 0,
                    right: -parseFloat(el.css('border-right-width')) || 0,
                    bottom: -parseFloat(el.css('border-bottom-width')) || 0,
                    left: -parseFloat(el.css('border-left-width')) || 0,
                    //background: 'red', opacity: 0.5,    // DEBUG
                    cursor: 'move'
                }
            }).appendTo(activeArea);
            activeArea.mousedown(e => dragging(e));

            this.elements = $([overlay]);
        }

        public disable() {
            this.elements.hide();
        }

        public enable() {
            this.elements.show();
        }

        public destroy() {
            this.elements.remove();
            this.elements = null;
        }
    }
}
