/// <reference path="../defs/jquery/jquery.d.ts" />
/// <reference path="Dom.ts" />

module letscard {

    export interface ContextMenuEntries {
        [key: string]: ContextMenu|ContextMenuEntries|(() => void);
    }

    export class ContextMenu {

        private static lastEvent: number;
        private static last: ContextMenu;

        private menu: JQuery;
        private child: ContextMenu;
        private rootParent: ContextMenu;

        private lastOpenLocation: { x: number; y: number; } = { x: 0, y: 0 };

        constructor(el: JQuery, entries?: ContextMenuEntries) {
            this.menu = dom.make('ul', {
                class: ['contextmenu'],
                style: {
                    position: 'fixed'
                }
            });

            if (el != null) {
                el.on('contextmenu', e => {
                    if (e.timeStamp == ContextMenu.lastEvent)
                        return;
                    ContextMenu.lastEvent = e.timeStamp;

                    this.openAt(e.clientY - 5, e.clientX - 5);

                    $(window).on('click', this.onClickOut);

                    e.preventDefault();
                });
            }

            if (entries)
                this.setEntries(entries);
        }

        public setEntries(entries: ContextMenuEntries) {
            this.menu.empty();

            for (var k in entries) {
                var expandable = typeof (entries[k]) != 'function' && entries[k] != null;

                var entry = dom.make('li', {
                    class: [
                        'contextmenu-entry',
                        entries[k] ? null : 'contextmenu-entry-disabled',
                        k.charAt(0) == '-' ? 'contextmenu-separator' : null,
                        expandable ? 'contextmenu-entry-expandable' : null
                    ],
                    text: k.charAt(0) == '-' ? null : k,
                    attr: {
                        tabIndex: entries[k] ? '0' : '-1'
                    }
                }).appendTo(this.menu);

                if (expandable) {
                    entry.on('mouseover click', ((entry: JQuery, entries: ContextMenu|ContextMenuEntries) => {
                        var menu = entries instanceof ContextMenu ? entries : new ContextMenu(null, <ContextMenuEntries>entries);
                        return (e: JQueryEventObject) => {
                            if (this.child)
                                this.child.close();

                            this.child = menu;

                            ContextMenu.lastEvent = e.timeStamp;

                            var offset = entry.offset();
                            menu.openAt(offset.top - window.scrollY, offset.left - window.scrollX + entry.outerWidth(), this.rootParent || this);
                        };
                    })(entry, <ContextMenu|ContextMenuEntries>entries[k]));
                } else {
                    entry.mouseover(() => {
                        if (this.child)
                            this.child.close();
                    });

                    entry.click(((entry: Function) => () => {
                        if (this.rootParent)
                            this.rootParent.close();
                        else
                            this.close();

                        if (entry)
                            entry();
                    })(<() => void>entries[k]));
                }
            }
        }

        public getOpenLocation(): { x: number; y: number; } {
            return this.lastOpenLocation;
        }

        private onClickOut = e => {
            //console.debug('ContextMenu.onClickOut');

            if (e.timeStamp == ContextMenu.lastEvent)
                return;

            if (this.contains(e.target))
                return;

            this.close();
        };

        private contains(e: HTMLElement) {
            return this.menu.is(e) || this.menu.has(e).length || (this.child && this.child.contains(e));
        }

        private openAt(top: number, left: number, extend?: ContextMenu) {
            //console.debug('ContextMenu.openAt', top, left, extend);

            if (!extend && ContextMenu.last != null) {
                ContextMenu.last.close();
                ContextMenu.last = null;
            }

            if (extend)
                this.rootParent = extend;

            this.lastOpenLocation = { x: left, y: top };
            this.menu.css({
                top: top,
                left: left
            });

            this.menu.appendTo(document.body);

            if (!extend)
                ContextMenu.last = this;
        }

        private close() {
            //console.debug("ContextMenu.close", 'this.child='+this.child);

            if (this.child) {
                this.child.close();
                this.child = null;
            }

            this.menu.detach();
            $(window).off('click', this.onClickOut);
        }
    }
}
