
module letscard {

    /** Provides access to web storage. */
    export class WebStorage {

        /** Storage persisting between sessions. */
        public static local = new WebStorage(false);

        /** Storage emptied when the browser is closed. */
        public static session = new WebStorage(true);

        private storage: Storage;

        constructor(session: boolean = false) {
            this.storage = session ? sessionStorage : localStorage;
        }

        /** Set a value with the specified key to the storage. */
		public set(key: string, value: any): void {
            this.storage.setItem(key, JSON.stringify(value));
		}

		/** Get a value from the storage. */
		public get(key: string): any {
			return JSON.parse(this.storage.getItem(key));
		}

        /** Delete a value from the storage. */
        public delete(key: string): void {
            return this.storage.removeItem(key);
        }

        /** Check if the storage contains a value with the specified key. */
        public contains(key: string): boolean {
            return this.storage.getItem(key) !== null;
        }
    }
}
