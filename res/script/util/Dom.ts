/// <reference path="../defs/jquery/jquery.d.ts" />

module letscard.dom {

    export interface DomElementProperties {
        class?: string[];
        attr?: { [attribute: string]: string };
        style?: { [attribute: string]: string|number };
        text?: string;
        child?: JQuery[];
        on?: { [event: string]: (e: JQueryEventObject) => void };
    }

    export function make(tag: string, args: DomElementProperties): JQuery {
        var el = $(document.createElement(tag));

        if (args.class)
            el.addClass(args.class.join(' '));

        if (args.attr)
            for (var k in args.attr)
                el.attr(k, args.attr[k]);

        if (args.style)
            el.css(args.style);

        if (args.text)
            el.text(args.text);

        if (args.child)
            for (var i = 0; i < args.child.length; i++)
                el.append(args.child[i]);

        if (args.on)
            for (var k in args.on)
                el.on(k, args.on[k]);

        return el;
    }

    export function placeholder(clazz?: string[]): JQuery {
        return make('div', {
            class: clazz,
            style: {
                position: 'absolute',
                top: 0,
                right: 0,
                bottom: 0,
                left: 0
            }
        });
    }
}
