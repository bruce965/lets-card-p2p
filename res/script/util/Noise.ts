/// <reference path="Random.ts" />

module letscard {

    /** Generate a noise pattern of the specified dimensions. */
    export function generateNoise(width: number, height: number, opacity: number) {
        var canvas = document.createElement('canvas');
        canvas.width = width;
        canvas.height = height;

        var ctx = <CanvasRenderingContext2D>canvas.getContext('2d');

        var random = new Random();
        random.seed(0);

        for (var y = 0; y < canvas.height; y++) {
            for (var x = 0; x < canvas.width; x++) {
                var number = random.intBetween(0, 255);
                ctx.fillStyle = "rgba(" + number + "," + number + "," + number + "," + opacity + ")";
                ctx.fillRect(x, y, 1, 1);
            }
        }

        return canvas.toDataURL('image/png');
    }

    export function generateHorizontalGradient(opacityStart: number, opacityEnd: number) {
        var canvas = document.createElement('canvas');
        canvas.width = 256;
        canvas.height = 1;

        var ctx = <CanvasRenderingContext2D>canvas.getContext('2d');

        for (var x = 0; x < canvas.width; x++) {
            ctx.fillStyle = "rgba(0,0,0," + (x / canvas.width * opacityStart + (1 - (x / canvas.width)) * opacityEnd) + ")";
            ctx.fillRect(x, 0, 1, canvas.height);
        }

        return canvas.toDataURL('image/png');
    }
}
