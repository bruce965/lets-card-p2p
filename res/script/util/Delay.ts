
module letscard {

    export class Delay {

        public onDelay: () => void;

        private timeout: number;
        private onDelayInstance = () => {
            this.timeout = null;
            this.onDelay();
        }

        constructor(public time: number = 200, public timeIsLimit: boolean = false) {

        }

        public set() {
            if(this.timeIsLimit && this.timeout != null)
                return;

            if(this.timeout != null)
                this.clear();

            this.timeout = setTimeout(this.onDelayInstance, this.time);
        }

        public clear() {
            if(this.timeout == null)
                return;

            clearTimeout(this.timeout);
            this.timeout = null;
        }
    }
}
