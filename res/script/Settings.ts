/// <reference path="defs/jquery/jquery.d.ts" />
/// <reference path="util/Delay.ts" />
/// <reference path="util/WebStorage.ts" />
/// <reference path="Card.ts" />

module letscard {

    export class Settings {

        private static settings = {
            table: {
                w: 0,
                h: 0,
                cards: <{ [id: string]: {
                    data: string,
                    x: number,
                    y: number,
                    flipped: boolean
                } }>{}
            },
            cardCategories: <{ [key: string]: CardCategory }>{},
            cards: <{ [key: string]: CardData }>{}
        };

        public settings: typeof Settings.settings = $.extend({}, Settings.settings);

        private saveSettings = new Delay(5000, true);

        constructor() {
            var settings = WebStorage.local.get('letscard_settings');
            if (settings)
                $.extend(this.settings, settings);

            this.saveSettings.onDelay = () => {
                console.log("Settings saved.");
                WebStorage.local.set('letscard_settings', this.settings);
            }
        }

        public backup(key: string) {
            var backups = WebStorage.local.get('letscard_backups') || {};
            backups[key] = true;
            WebStorage.local.set('letscard_backups', backups);

            WebStorage.local.set('letscard_backup_' + key, this.settings);
        }

        public restore(key: string) {
            for (var k in this.settings)
                delete this.settings[k];

            $.extend(this.settings, WebStorage.local.get('letscard_backup_' + key));
        }

        public deleteBackup(key: string) {
            var backups = WebStorage.local.get('letscard_backups') || {};
            delete backups[key];
            WebStorage.local.set('letscard_backups', backups);

            WebStorage.local.delete('letscard_backup_' + key);
        }

        public listBackups(): string[] {
            var backups = WebStorage.local.get('letscard_backups') || {};
            var keys: string[] = [];

            for (var k in backups)
                keys.push(k);

            return keys;
        }

        public reset() {
            for (var k in this.settings)
                delete this.settings[k];

            $.extend(this.settings, Settings.settings);
        }

        public save(now?: boolean) {
            if (now)
                this.saveSettings.onDelay();
            else
                this.saveSettings.set();
        }
    }
}
