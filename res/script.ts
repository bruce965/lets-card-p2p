/// <reference path="script/util/Delay.ts" />
/// <reference path="script/util/Uid.ts" />
/// <reference path="script/util/WebStorage.ts" />
/// <reference path="script/Card.ts" />
/// <reference path="script/Networking.ts" />
/// <reference path="script/Settings.ts" />
/// <reference path="script/Table.ts" />

module letscard {

    $(() => {
        var settingsManager = new Settings();
        var settings = settingsManager.settings;
        var save = (setting?: any, data?: any) => {
            if (data) $.extend(setting, data);
            settingsManager.save();
        };

        var cards: { [id: string]: Card } = {};

        var networking = window['debug_networking'] = new Networking();
        networking.onMessage = (sender, data) => {
            var self = sender == null;

            //console.debug("Incoming message from " + self ? 'self' : sender.peer, data);
            switch (data.action) {
                case 'resizeTable':
                    save(settings.table, data.table);
                    table.setSize(settings.table.w, settings.table.h);
                    break;
                case 'spawnCard':
                    spawnCard(data.id, data.card.data, data.card.x, data.card.y, data.card.flipped);
                    break;
                case 'moveCard':
                    var card = $.extend(settings.table.cards[data.id], { x: data.x, y: data.y });
                    delete settings.table.cards[data.id];
                    settings.table.cards[data.id] = card;
                    if (!self) cards[data.id].setPosition(data.x, data.y);
                    break;
                case 'flipCard':
                    var card = $.extend(settings.table.cards[data.id], { flipped: data.flipped });
                    delete settings.table.cards[data.id];
                    settings.table.cards[data.id] = card;
                    save();
                    if (!self) cards[data.id].flip(data.flipped, false, false);
                    break;
                case 'removeCard':
                    delete settings.table.cards[data.id];
                    if (!self) cards[data.id].remove(false);
                    delete cards[data.id];
                    save();
                    break;
                case 'resetTable':
                    settingsManager.reset();
                    reloadEverything();
                    break;
                case 'loadSettings':
                    settingsManager.reset();
                    save(settings, data.settings);
                    reloadEverything();
                    break;
            }
        };

        var spawnCard = (id: string, key: string, x: number, y: number, flipped: boolean, manually?: boolean) => {
            //console.debug("spawnCard", id, key, x, y, flipped, manually);

            var data = settings.cards[key];
            var category = settings.cardCategories[data.category];
            var card = new Card(table, category, data);
            cards[id] = card;
            card.setPosition(x, y);
            card.flip(flipped, true, false);

            var cardObject = {};
            cardObject[id] = { data: key, x: x, y: y, flipped: flipped };
            save(settings.table.cards, cardObject);

            ((id: string, card: Card) => {
                card.onMove = (x: number, y: number) => {
                    networking.send({ action: 'moveCard', id: id, x: x, y: y }, true);
                };
                card.onFlip = (flipped: boolean) => {
                    networking.send({ action: 'flipCard', id: id, flipped: flipped }, true);
                };
                card.onRemove = () => {
                    networking.send({ action: 'removeCard', id: id }, true);
                };
            })(id, card);

            if (manually)
                networking.send({ action: 'spawnCard', id: id, card: cardObject[id] });
        };

        var table = new Table();
        table.onResize = (width, height) => {
            save(settings.table, { w: width, h: height })
            networking.send({ action: 'resizeTable', table: settings.table });
        };
        table.onAddFriend = () => networking.addFriend();
        table.onNewGame = () => alert("Function not implemented yet, please load a table instead.");
        table.onSpawnCard = (x: number, y: number, key: string) => {
            spawnCard(uid(), key, x, y, true);
        };
        table.onAddCardCategory = (name: string, category: CardCategory) => {
            settings.cardCategories[name] = category;
            table.setCardCategories(settings.cardCategories);
            save();
        };
        table.onAddCard = (name: string, card: CardData) => {
            settings.cards[name] = card;
            table.setCards(settings.cards);
            save();
        };
        table.onRemoveCardCategory = (key: string) => {
            delete settings.cardCategories[key];
            table.setCardCategories(settings.cardCategories);
            save();
        };
        table.onRemoveCard = (key: string) => {
            delete settings.cards[key];
            table.setCards(settings.cards);
            save();
        };
        table.onSaveTable = (key: string) => {
            settingsManager.backup(key);
            table.setTables(settingsManager.listBackups());
        };
        table.onLoadTable = (key: string) => {
            settingsManager.restore(key);
            reloadEverything();
            networking.send({ action: 'loadSettings', settings: settings });
        };
        table.onDeleteTable = (key: string) => {
            settingsManager.deleteBackup(key);
            table.setTables(settingsManager.listBackups());
        };
        table.onResetTable = () => {
            networking.send({ action: 'resetTable' }, true);
        };

        var reloadEverything = () => {
            table.clear();
            table.setSize(settings.table.w, settings.table.h);
            table.setCardCategories(settings.cardCategories);
            table.setCards(settings.cards);
            table.setTables(settingsManager.listBackups());

            cards = {};
            for (var id in settings.table.cards) {
                var card = settings.table.cards[id];
                spawnCard(id, card.data, card.x, card.y, card.flipped, false);
            }
        };
        reloadEverything();
    });
}
