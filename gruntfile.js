
module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		copy: {
			dev: {
				files: [{
					expand: true,
					cwd: '',
					src: ['**/*', "!node_modules/**", "!build/**", "!package.json", "!gruntfile.js"],
					dest: 'build/'
				}]
			}
		},
		
		clean: {
			dev: {
				src: ['build/']
			}
		},
		
		tslint: {
			options: {
				configuration: {
				}
			},
			all: {
				src: ["**/*.ts", "!node_modules/**/*.ts", "!build/**/*.ts", "!typings/**/*.ts"]
			}
		},

		ts: {
			dev: {
				src: ["**/*.ts", "!node_modules/**/*.ts", "!build/**/*.ts"],
				dest: "build/res/script.js",
				options: {
					fast: 'never'
				}
			}
		},

		less: {
			dev: {
				options: {
					paths: ["."]
				},
				files: {
					"build/res/style.css": "res/style.less"
				}
			}
		},
		
		watch: {
			scripts: {
				files: ["**/*.ts", "!node_modules/**", "!build/**"],
				tasks: ['newer:tslint:all', 'ts:dev'],
				options: {
					spawn: false
				}
			},
			styles: {
				files: ["**/*.less", "!node_modules/**", "!build/**"],
				tasks: ['less:dev'],
				options: {
					spawn: false
				}
			},
			resources: {
				files: ["**/*", "!node_modules/**", "!build/**"],
				tasks: ['diffCopy:dev'],
				options: {
					event: ['added', 'changed', 'renamed'],
				}
			}
		},
		
		concurrent: {
			dev: {
				tasks: ['connect:dev', 'watch:scripts', 'watch:styles', 'watch:resources'],
				options: {
					logConcurrentOutput: true
				}
			}
		},
		
		connect: {
			dev: {
				port: 8080,
				base: 'build/'
			}
		}
	});

	grunt.loadNpmTasks("grunt-tslint");
	grunt.loadNpmTasks("grunt-ts");
	grunt.loadNpmTasks("grunt-contrib-less");
	grunt.loadNpmTasks("grunt-newer");
	grunt.loadNpmTasks("grunt-contrib-watch");
	grunt.loadNpmTasks("grunt-contrib-copy");
	grunt.loadNpmTasks("grunt-contrib-clean");
	grunt.loadNpmTasks("grunt-nodemon");
	grunt.loadNpmTasks("grunt-concurrent");
	grunt.loadNpmTasks('grunt-diff-copy');
	grunt.loadNpmTasks('grunt-connect');
	
	grunt.registerTask('build:dev', ['diffCopy:dev', 'newer:tslint:all', 'ts:dev', 'less:dev']);
	grunt.registerTask('default', ['build:dev', 'concurrent:dev']);
	// task: clean
};
